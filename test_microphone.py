#!/usr/bin/env python3

import sys
import os
import vosk
from queue import Queue
from pyaudio import PyAudio, Stream, paInt16, paContinue

queue = Queue()

def _data_callback( in_data, frame_count, time_info, status ):
    # process in_data with frame_count frames 
    print( f'{frame_count} frames read (status:{status})' )
    queue.put( bytes(in_data) )
    return (None, paContinue) # output data + status

device = PyAudio()

try:
    model = vosk.Model( "vosk-model-small-pt-0.3" )

    stream = device.open( format=paInt16,
                          channels=1,
                          rate=44100,
                          input=True,
                          frames_per_buffer=44100,
                          stream_callback=_data_callback )

    stream.start_stream()

    rec = vosk.KaldiRecognizer( model, 44100, '["liga", "desliga", "inicia", "para", "[unk]"]' )
    while True:
        data = queue.get()
        if rec.AcceptWaveform(data):
            print(rec.Result())
        else:
            print(rec.PartialResult())

except KeyboardInterrupt:
    print( '\nDone' )  
except Exception as e:
    print( type(e).__name__ + ': ' + str(e) )

print( rec.FinalResult() )
stream.stop_stream()
stream.close()
device.terminate()